CREATE DATABASE IF NOT EXISTS `dev_db_1` COLLATE 'utf8_general_ci' ;
GRANT ALL ON `dev_db_1`.* TO 'default'@'%' ;

FLUSH PRIVILEGES ;

CREATE TABLE `dev_db_1`.post (
    id INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    title VARCHAR(255),
    created date,
    creator VARCHAR(255)
);
