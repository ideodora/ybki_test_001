import pkg_resources
from webob import dec, static
from webdispatch import URLDispatcher
from jinja2 import Environment, PackageLoader
from wsgiref.simple_server import make_server
import MySQLdb

# serve static
static_resource = pkg_resources.resource_filename(__name__, 'static')
static_app = static.DirectoryApp(static_resource)

# template env
env = Environment(loader=PackageLoader('app', 'templates'))

# handlers#1
@dec.wsgify
def index(request):

    if request.method == 'POST':
        template = env.get_template('index.html')

        title = request.params.get('name', 'canai?')

        # データベース接続とカーソル生成
        # 接続情報はダミーです。お手元の環境にあわせてください。
        connection = MySQLdb.connect(
            host='mysql',
            user='default',
            passwd='secret',
            db='dev_db_1',
            charset='utf8'
        )
        cursor = connection.cursor()

        cursor.execute("INSERT INTO post (title) VALUES (%s)", (title,))
        cursor.execute('SELECT * FROM post ORDER BY id ASC')

        posts = cursor.fetchall()

        # 保存を実行（忘れると保存されないので注意）
        connection.commit()

        # 接続を閉じる
        connection.close()

        dto = {
            'site': 'Awesome site',
            'name': 'Agus Makmun',
            'posts': posts
        }
        return template.render(dto)
    else:
        template = env.get_template('index.html')

        # データベース接続とカーソル生成
        # 接続情報はダミーです。お手元の環境にあわせてください。
        connection = MySQLdb.connect(
            host='mysql',
            user='default',
            passwd='secret',
            db='dev_db_1',
            charset='utf8'
        )
        cursor = connection.cursor()

        cursor.execute('SELECT * FROM post ORDER BY id ASC')

        posts = cursor.fetchall()

        # 保存を実行（忘れると保存されないので注意）
        connection.commit()

        # 接続を閉じる
        connection.close()

        dto = {
            'site': 'Awesome site',
            'name': 'Agus Makmun',
            'posts': posts
        }
        return template.render(dto)

# handlers#2
@dec.wsgify
def detail(request):

    title = request.params.get('name', 'canai?')

    # データベース接続とカーソル生成
    # 接続情報はダミーです。お手元の環境にあわせてください。
    connection = MySQLdb.connect(
        host='mysql',
        user='default',
        passwd='secret',
        db='dev_db_1',
        charset='utf8'
    )
    cursor = connection.cursor()

    cursor.execute("INSERT INTO post (title) VALUES (%s)", (title,))
    # cursor.execute('SELECT * FROM post ORDER BY id ASC')

    # record = cursor.fetchone()

    # 保存を実行（忘れると保存されないので注意）
    connection.commit()

    # 接続を閉じる
    connection.close()

    template = env.get_template('detail.html')
    dto = {
        'site': 'Awesome site',
        'name': 'Agus Makmun',
    }
    return template.render(dto)

# routing
app = URLDispatcher()
app.add_url('static', '/static/*', static_app)
app.add_url('index', '/', index)
app.add_url('detail', '/detail', detail)

# serve server
print("Listening on port 8080...")
httpd = make_server('', 8080, app)
httpd.serve_forever()
